package com.ivmagl.servlets;

import com.google.gson.Gson;
import com.ivmagl.entities.Task;
import com.ivmagl.service.TasksRepository;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ApiServlet extends HttpServlet {
    private TasksRepository tasksRepository;

    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
        tasksRepository = new TasksRepository();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Task> listOfTasks = tasksRepository.getTasksFromStorage();
        String json = new Gson().toJson(listOfTasks);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        Map<String, String[]> taskInfo = request.getParameterMap();
        String taskDescription = Arrays.asList(taskInfo.get("description")).get(0);
        if(id == null) {
            tasksRepository.addTaskToStorage(taskDescription);
        } else {
            Integer mainTaskId = parseInt(id);
            if(mainTaskId != null) {
                tasksRepository.addSubtask(mainTaskId, taskDescription);
            }
        }
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer id = parseInt(request.getPathInfo().replaceAll("/", ""));
        if(id != null) {
            tasksRepository.deleteTaskFromStorage(id);
        }
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Task obj = getTaskFromJson(request);
        if(obj != null) {
            tasksRepository.updateTask(obj);
        }
        response.setStatus(HttpServletResponse.SC_OK);
    }

    private Task getTaskFromJson(HttpServletRequest request) {
        StringBuilder jb = new StringBuilder();
        String line;
        Task task = null;
        try (BufferedReader reader = request.getReader()){
            while ((line = reader.readLine()) != null) {
                jb.append(line);
            }
            task = new Gson().fromJson(jb.toString(), Task.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return task;
    }

    private Integer parseInt(String string) {
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
