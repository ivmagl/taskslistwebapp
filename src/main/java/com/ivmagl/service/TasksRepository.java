package com.ivmagl.service;

import com.ivmagl.entities.Task;

import java.util.List;

public class TasksRepository {
    private final Task storage;
    private static int id = 0;

    public TasksRepository() {
        storage = new Task();
    }

    public List<Task> getTasksFromStorage() {
        return storage.getSubTasksList();
    }

    public void addTaskToStorage(String description) {
        storage.addToSubTasksList(new Task(generateId(), description));
    }

    public void deleteTaskFromStorage(int id) {
        deleteTask(storage.getSubTasksList(), id);
    }

    public void updateTask(Task task){
        Task taskFromStorage = storage.getTaskById(task.getId());
        if(taskFromStorage != null) {
            taskFromStorage.setDescription(task.getDescription());
        }
    }

    public void addSubtask(int id, String description) {
        Task taskFromStorage = storage.getTaskById(id);
        if(taskFromStorage != null) {
            taskFromStorage.addToSubTasksList(new Task(generateId(), description));
        }
    }

    private void deleteTask(List<Task> toDosList, int id) {
        for (Task task : toDosList) {
            if (task.getId() == id) {
                toDosList.remove(task);
                return;
            }
            if(task.getSubTasksList() != null) {
                deleteTask(task.getSubTasksList(), id);
            }
        }
    }

    private static synchronized int generateId() {
        id++;
        return id;
    }
}
