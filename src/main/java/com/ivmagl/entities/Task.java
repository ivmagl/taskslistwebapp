package com.ivmagl.entities;

import java.util.LinkedList;
import java.util.List;

public class Task {
    private int id;
    private String description;
    private List<Task> subTasksList;

    public Task(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public Task() {

    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Task> getSubTasksList() {
        return subTasksList;
    }

    public synchronized void addToSubTasksList(Task task) {
        if (subTasksList == null) {
            subTasksList = new LinkedList<>();;
        }
        subTasksList.add(task);
    }

    public Task getTaskById(int id) {
        if(this.id == id) {
            return this;
        } else {
            if(subTasksList != null){
                for(Task task : subTasksList) {
                    Task taskFromSubList = task.getTaskById(id);
                    if(taskFromSubList != null)
                        return taskFromSubList;
                }
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (id != task.id) return false;
        return description.equals(task.description);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + description.hashCode();
        return result;
    }
}
