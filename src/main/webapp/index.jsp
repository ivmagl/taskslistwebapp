<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>To-dos List</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/buttonEventInit.js"></script>
    <link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>
<div class="container">
    <form id="mainForm" action="" method="POST">
        <label for="description">Add Task</label>
        <input id="description" type="text" autocomplete="off" maxlength="200">
        <input type="submit" value="Add" id="submitTask">
    </form>
    <h3>Todo</h3>
    <div class="container2" id="listOfTasks"></div>
</div>
</body>
</html>