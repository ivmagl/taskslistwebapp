var API_TODO_URI = 'api/todo/';

$(document).ready(function () {
    $("#mainForm").submit(function (event) {
        event.preventDefault();
        var taskDescription = $("#description").val();

        if (isEmptyDescription(taskDescription) === false) {
            $.post(API_TODO_URI, {description: taskDescription}, function () {
                $("#description").val('');

                showTasks();
            });
        }
    });
    showTasks();
});

function showTasks() {
    $.get(API_TODO_URI, function (response) {
        $("#listOfTasks").empty();
        iterateTasks(response);
    });
}

function deleteTask(id) {
    $.ajax({
        type: 'DELETE',
        url: API_TODO_URI + id,
        success: function() {
            showTasks()
        }
    });
}

function updateTask(id) {
    var description = prompt("New description: ", " ");
    if(isEmptyDescription(description) === false) {
        var data = {id: id, description: description};
        $.ajax({
            type: 'PUT',
            url: API_TODO_URI,
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function () {
                showTasks()
            }
        });
    }
}

function addSubtask(id) {
    var subtaskDescription = prompt("Subtask description: ", " ");
    if (isEmptyDescription(subtaskDescription) === false) {
        $.post(API_TODO_URI, {id: id, description: subtaskDescription}, function () {
            showTasks();
        });
    }
}

function iterateTasks(tasks) {
    $.each(tasks, function (index, task) {
        var $ul = $("<ul>").appendTo($("#listOfTasks"));
        printTasks(task.id, task.description, $ul);
        if(task.subTasksList === undefined) {}
        else {
            iterateSubtasks(task.subTasksList, $ul);
        }
    });
}

function iterateSubtasks(listOfTasks, $ul) {
    var $subUl = $("<ul>").appendTo($ul);
    $.each(listOfTasks, function (index, subtask) {
        printTasks(subtask.id, subtask.description, $subUl);
        if(subtask.subTasksList === undefined) {}
        else {
            iterateSubtasks(subtask.subTasksList, $subUl);
        }
    });
}

function printTasks(id, description, $ul) {
    var spanText = $('<span\>').html(unescape(description));
    $("<li>").appendTo($ul)
        .append(spanText)
        .append("<br/>")
        .append(generateButtonTag("Update").click(function () {updateTask(id);}))
        .append(generateButtonTag("Delete").click(function () {deleteTask(id);}))
        .append(generateButtonTag("Add subtask").click(function () {addSubtask(id);}))
        .append(" <br/><br/>");
}

function generateButtonTag(value) {
    return $('<input />', {type  : 'button', value : value});
}

function unescape(description) {
    description = description.replace(/&/g, '&amp;');
    description = description.replace(/</g, '&lt;');
    description = description.replace(/>/g, '&gt;');
    description = description.replace(/"/g, '&quot;');
    return description;
}

function isEmptyDescription(description) {
    if($.trim(description) === '') {
        alert('You must write something!');
        return true;
    } else {
        return false;
    }
}

